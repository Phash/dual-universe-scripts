-- erste Hitpointanzeige

max = 0
current = 0
for k,v in pairs(core.getElementIdList()) do
        if (core.getElementNameById(k) ~= "") then
        max = max + core.getElementMaxHitPointsById(k)
        current = current + core.getElementHitPointsById(k) 
	system.print(string.format("Element Name: %s, Type: %s - max/cur/per: %.2f/%.2f/%.0f",
            core.getElementNameById(k),
            core.getElementTypeById(k),
            core.getElementHitPointsById(k),
            core.getElementMaxHitPointsById(k),
            (core.getElementMaxHitPointsById(k) / core.getElementHitPointsById(k)) * 100
            ))
        end
end

system.print(string.format("Max: %.0f - Current: %.0f - Percent: %.0f", max, current, max / current * 100))




function getHP() 
        data = {}
        
        max = 0
        current = 0
        for k,v in pairs(core.getElementIdList()) do
                if (core.getElementNameById(k) ~= "") then
                        max = max + core.getElementMaxHitPointsById(k)
                        current = current + core.getElementHitPointsById(k) 
                end
        end
            data["max"] = max
         data["current"] = current
         data["percent"] = max / current * 100
        return data
    end
    
    
    
    hppanel = system.createWidgetPanel("HitPoints") -- panelID
    hpwidget = system.createWidget(hppanel, "hpwidget") -- widgetID
    unit.setTimer("hp", 1)
    --system.print("----"..json.encode(getHP()))
    dataHP = system.createData(json.encode(getHP()))
    system.addDataToWidget(datahp, hpwidget)
    
    
system.print (json.encode(getHP()))
dataHP = system.createData(json.encode(getHP()))
system.updateData(datahp, hpwidget)

html = [[ 
<style>
.red{color:red}
.left{margin-left:0px}
.right{margin-right:0px; float:right}
</style>
<div class="monitor_left window" style="width:20%; font-size:2em;  >
<div class="center window" style="margin-left: 5%;" >
]]
data = getHP()
alert = ""
if data.percent < 20 then alert = "red" end
html = html .. string.format("<div class='left ".. alert .."'>Max: <span class='right'>%.0f</span></div>", data.max)
html = html .. string.format("<div class='left ".. alert .."'>Current: <span class='right'>%.0f</span></div>", data.current)
html = html .. string.format("<div class='left ".. alert .."'>Percent: <span class='right'>%.0f</span></div>", data.percent)

html = html .. [[
</div>
</div>
]]

system.setScreen(html)
system.showScreen(1)
    
    