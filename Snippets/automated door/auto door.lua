-- backup 
{"slots":{"0":{"name":"button","type":{"events":[],"methods":[]}},"1":{"name":"door","type":{"events":[],"methods":[]}},"2":{"name":"slot3","type":{"events":[],"methods":[]}},"3":{"name":"slot4","type":{"events":[],"methods":[]}},"4":{"name":"slot5","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"door.activate()\nunit.setTimer(\"doorclosetimer\", 3)","filter":{"args":[],"signature":"pressed()","slotKey":"0"},"key":"0"},{"code":"door.deactivate()","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"door.deactivate()\nunit.stopTimer(\"doorclosetimer\")","filter":{"args":[{"value":"doorclosetimer"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"2"}],"methods":[],"events":[]}

--
-- Video: https://www.youtube.com/watch?v=_QyYHLxXgG0

-- requiers
-- - door named door
-- - button named button

-- unit.start
door.deactivate()

-- unit.tick(doorclosetimer)
door.deactivate()
unit.stopTimer("doorclosetimer")

-- door.pressed()
door.activate()
unit.setTimer("doorclosetimer", 3)