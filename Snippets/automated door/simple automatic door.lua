-- backup 
{"slots":{"0":{"name":"door","type":{"events":[],"methods":[]}},"1":{"name":"buttonaussen","type":{"events":[],"methods":[]}},"2":{"name":"laser","type":{"events":[],"methods":[]}},"3":{"name":"container_1","type":{"events":[],"methods":[]}},"4":{"name":"container_2","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"unit.setTimer(\"doorclose\" , 3)\ndoor.toggle()\n","filter":{"args":[],"signature":"pressed()","slotKey":"1"},"key":"0"},{"code":"closing = false\nfunction closeDoor()\n    if closing == false then\n        closing = true\n        unit.setTimer(\"close\", 3)\n    end\nend","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"door.deactivate()\nclosing = false","filter":{"args":[{"value":"close"}],"signature":"tick(timerId)","slotKey":"-1"},"key":5},{"code":"if door.getState() == 1 then\n    closeDoor()\nend","filter":{"args":[],"signature":"update()","slotKey":"-2"},"key":"2"},{"code":"","filter":{"args":[],"signature":"start()","slotKey":"-2"},"key":"3"}],"methods":[],"events":[]}
-- in factory
{"slots":{"0":{"name":"button","type":{"events":[],"methods":[]}},"1":{"name":"door","type":{"events":[],"methods":[]}},"2":{"name":"slot3","type":{"events":[],"methods":[]}},"3":{"name":"slot4","type":{"events":[],"methods":[]}},"4":{"name":"slot5","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"door.activate()\nunit.setTimer(\"doorclosetimer\", 3)","filter":{"args":[],"signature":"pressed()","slotKey":"0"},"key":"0"},{"code":"closing = false\nfunction closeDoor()\n    if closing == false then\n        closing = true\n        unit.setTimer(\"close\", 3)\n    end\nend","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"1"},{"code":"door.deactivate()\nclosing = false\nunit.stopTimer(\"close\")","filter":{"args":[{"value":"close"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"2"},{"code":"if door.getState() == 1 then\n    closeDoor()\nend","filter":{"args":[],"signature":"update()","slotKey":"-2"},"key":"3"}],"methods":[],"events":[]}


-- unit.tick(close)
door.deactivate()
closing = false

-- unit.start()
closing = false
function closeDoor()
    if closing == false then
        closing = true
        unit.setTimer("close", 3)
    end
end
-- system.update()

if door.getState() == 1 then
    closeDoor()
end