local current = 50
local idx = 1
local id = "but"

local buttons = {}

function getBaseHtml() 
    local html = ""
    --sgui:createButtonArea(screen, 0.3, 0.9, 0.1, 0.1, "+", inc)
    --sgui:createButtonArea(screen, 0.6, 0.9, 0.1, 0.1, "-", dec)
    
    button1 = createButton(id .. idx, 0.3, 0.8, 0.1, 0.1, "+", inc)
    buttons[idx] = button1
    html = html .. button1
    idx = idx + 1
    button2 = createButton(id .. idx, 0.6, 0.8, 0.1, 0.1, "-", dec)
    buttons[idx] = button2
    html = html .. button2
    idx = idx + 1
    return html
    end
function getSVG() 
   -- getBaseHtml()
    local h =[[
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1810px" height="840px" viewBox="0 0  1810 840" preserveAspectRatio="xMidYMid meet" >
            <rect id="svgEditorBackground" x="0" y="0" width="1810" height="840" style="fill: none; stroke: none;"/>
            <rect x="45vw" y="20vh" style="fill:green;stroke:black;stroke-width:1px;" id="e1_rectangle" width="10vw" height="]]..current..[["/>
        </svg>
	]]
   return h
    end
function createButton(id, x, y, hx, hy, text, fun)
    button = "<div class='bootstrap' style='background:#FF0000;width:".. (hx*100) ..
	         "vw;height:" .. (hy*100) .."vh;overflow:hidden;font-size:".. (hy*50) ..
	         "vh; position:fixed; left: "..(x*100).."vw; top:"..(y*100).."vh;'>" .. text .. "</div>"
	--id = screen.setContent(x * 100, y * 100, button)
 
    sgui:createClickableArea(id, x, y, hx, hy, fun)
    return button
end
    
function initUI()
        local html = "<div style='font-size:3em'>CurrentValue:".. current .."</div>" ..getBaseHtml()
        html = html .. getSVG()
        screen.setHTML(html)
    end
function updateUI()
    local html = "<div style='font-size:3em'>CurrentValue:".. current .."</div>"
     for k,v in ipairs (buttons) do
        html = html .. v
        end
     html = html ..getSVG()
     screen.setHTML(html)
    end
function inc() 
   if current < 100 then current = current + 1 end
    updateUI()
end
    
function dec() 
   if current > 0 then current = current - 1 end
    updateUI()
end
   
screen.clear()
initUI()

  