--tick.updateDB
data = {}
data["bereich"] = ident
dataCont = {}
data["container"] = dataCont
dataCont["Hematite"] = {mass=hematite.getItemsMass(), max=166400*3, density = 5.04 }
dataCont["Coal"] = {mass=coal.getItemsMass(), max=166400*2, density = 1.35 }
dataCont["Bauxite"] = {mass=bauxite.getItemsMass(), max=166400*2, density = 1.28 }
dataCont["Quartz"] = {mass=quartz.getItemsMass(), max=166400*2, density = 2.65 }

dataAsString = json.encode(data)

db.setStringValue(ident, dataAsString)

--unit.start
ident = "ResourcesT1"

unit.setTimer("updateDB", 3)



data = {}
data["bereich"] = ident
dataCont = {}
data["container"] = dataCont
dataCont[1] = {name ='Pyrite', mass=pyrite.getItemsMass(), max=166400*3, density = 5.04 }
dataCont[2] = {name ='Petalite',mass=petalite.getItemsMass(), max=166400*2, density = 1.35 }
dataCont[3] = {name ='Garnierite',mass=garnierite.getItemsMass(), max=166400*2, density = 1.28 }
dataCont[4] = {name ='Acanthite',mass=acanthite.getItemsMass(), max=166400*2, density = 2.65 }

dataAsString = json.encode(data)

db.setStringValue(ident, dataAsString)
-- T3
data = {}
data["bereich"] = ident
dataCont = {}
data["container"] = dataCont
dataCont[1] = {name ='Pyrite', mass=pyrite.getItemsMass(), max=166400*3, density = 5.01 }
dataCont[2] = {name ='Petalite',mass=petalite.getItemsMass(), max=166400*2, density = 2.41 }
dataCont[3] = {name ='Garnierite',mass=garnierite.getItemsMass(), max=166400*2, density = 2.6 }
dataCont[4] = {name ='Acanthite',mass=acanthite.getItemsMass(), max=166400*2, density = 7.2 }

dataAsString = json.encode(data)

db.setStringValue(ident, dataAsString)


--T2
data = {}
data["bereich"] = ident
dataCont = {}
data["container"] = dataCont
dataCont[1] = {name ='Natron', mass=natron.getItemsMass(), max=166400*3, density = 5.01 }
dataCont[2] = {name ='Malachite',mass=malachite.getItemsMass(), max=166400*2, density = 2.41 }
dataCont[3] = {name ='Limestone',mass=limestone.getItemsMass(), max=166400*2, density = 2.6 }
dataCont[4] = {name ='Chromite',mass=chromite.getItemsMass(), max=166400*2, density = 7.2 }

dataAsString = json.encode(data)

db.setStringValue(ident, dataAsString)