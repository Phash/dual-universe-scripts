list = json.decode( db.getKeys())
local color = "black ;"
html = [[
<style>
.w3-container,
  .w3-panel {
    padding: 0.01em 16px;
  }
  .w3-panel {
    margin-top: 16px;
    margin-bottom: 16px;
  }
  .w3-green,
  .w3-hover-green:hover {
    color: #fff !important;
    background-color: #4caf50 !important;
  }
  .w3-red,
  .w3-hover-red:hover {
    color: #fff !important;
    background-color: #f44336 !important;
  }
  .w3-yellow,
  .w3-hover-yellow:hover {
    color: #000 !important;
    background-color: #ffeb3b !important;
  }
  .w3-center {
    text-align: center !important;
  }
body {
  background-image: url('assets.prod.novaquark.com/58139/a0a98231-07bc-4577-bbc6-2f6b361cc4e1.jpg');
  background-repeat: no-repeat; 
  background-attachment: fixed;
  background-position: center top;
  background-size: 300px ;
}
</style>
<div class="bootstrap">
<h1 style="
	font-size: 4em;
">Hadez Coperation</h1>
<table style="
	margin-left: 2%;
	width: 90%;
	font-size: 2em;">
<thead>
	<tr style="background-color: red;">

		<th style='width:20%; margin-left: 10px;'>Ore</th>
		<th style='width:20%'>Qty</th>
		<th style='width:20%'>Levels</th>
		<th style='width:20%'>Status</th>
		<th style='width:20%'>Available</th>
</thead>
</tr>
]]
for k,v in pairs(list) do
	data = json.decode (db.getStringValue(v))
    for key, value in pairs(data["container"]) do
    html = html .."<tr>" 
        html = html .."<td>" .. key
        html = html .."</td>"
        html = html .."<td>" .. string.format('%.0f', value.mass)
        html = html .."</td>"
        html = html .."<td>" .. string.format('%.0f',value.mass / value.density)
        html = html .."</td>"
        html = html .."<td>" .. string.format('%.0f',value.max - (value.mass / value.density))
        html = html .."</td>"
    available =  (value.mass / value.density / value.max * 100)
        html = html .."<td>" 
        html = html .. "<div style='height: 35px; width:100%'>" 
            html = html .."<div class='w3-container " 
        if available < 25 and available >10 then
            html = html .."w3-yellow "
            color = "black"
        elseif available < 10 then
        	html = html .."w3-red "
        	color = "white"
        else
        	html = html .."w3-green "
        	color = "white"
	   end
    	   html = html .."w3-center' style='height: 20px; padding: 2px; width: "
    		html = html .. string.format('%.0f',available) .."%; "
    		html = html .. "color: " .. color .. " '>"
    		html = html .. string.format('%.0f',available)
            html = html .. "</div>" 
        html = html .. "</div>" 
        html = html .."</td>"
    html = html .."</tr>" 
end
end

html = html .. [[
</table>
</div>
]]

screen.setHTML(html)