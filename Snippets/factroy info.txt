-- Factory overview
html =  '<div> Fallen Factory </div>'

-- DB init --
if db.hasKey("aluminium") == 0 then db.setIntValue("aluminium", 0) end
if db.hasKey("ganerite") == 0 then db.setIntValue("ganerite", 0) end

function machineStateUpdate()
	updateScreen()
end

function aluFertig()    
	temp = db.getIntValue("aluminium") + 1
     db.setIntValue("aluminium", temp)       
	updateScreen()
end
function ganeriteFertig()
	temp = db.getIntValue("ganerite") + 1
     db.setIntValue("ganerite", temp)
	updateScreen()
end
 
function updateDB()
    dbStatus.setStringValue("smelter_alu_1", smelter_alu_1.getStatus()) 
    dbStatus.setStringValue("smelter_alu_2", smelter_alu_2.getStatus()) 
end

function updateScreen()
    updateDB()
	html = '<div> Fallen Factory </div>' 
     local alu =  db.getIntValue("aluminium")
	local ganerite =  db.getIntValue("ganerite")
    html = html .. '<div>ALUMINIUM geschmolzen ('.. alu ..')</div> ' 
    html = html .. '<div>GANERITE geschmolzen ('.. ganerite ..')</div>' 
    if smelter_alu_1.getStatus() ~= "RUNNING" then
     	html = html .. '<div>smelter_alu_1 has stopped working: <span> '.. smelter_alu_1.getStatus() .. '</span> </div>'
    end
      if smelter_alu_2.getStatus() ~= "RUNNING" then
     	html = html .. '<div>smelter_alu_2 has stopped working: <span> '.. smelter_alu_2.getStatus() .. '</span> </div>'
    end
      if smelter_1.getStatus() ~= "RUNNING" then
     	html = html .. '<div>smelter_ganerite_1 has stopped working: <span> '.. smelter_1.getStatus() .. '</span> </div>'
    end
	sof
end

    