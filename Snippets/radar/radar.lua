-- backup
{"slots":{"0":{"name":"core","type":{"events":[],"methods":[]}},"1":{"name":"db","type":{"events":[],"methods":[]}},"2":{"name":"slot3","type":{"events":[],"methods":[]}},"3":{"name":"slot4","type":{"events":[],"methods":[]}},"4":{"name":"slot5","type":{"events":[],"methods":[]}},"5":{"name":"slot6","type":{"events":[],"methods":[]}},"6":{"name":"slot7","type":{"events":[],"methods":[]}},"7":{"name":"slot8","type":{"events":[],"methods":[]}},"8":{"name":"slot9","type":{"events":[],"methods":[]}},"9":{"name":"slot10","type":{"events":[],"methods":[]}},"10":{"name":"slot11","type":{"events":[],"methods":[]}},"11":{"name":"slot12","type":{"events":[],"methods":[]}},"12":{"name":"slot13","type":{"events":[],"methods":[]}},"13":{"name":"slot14","type":{"events":[],"methods":[]}},"14":{"name":"slot15","type":{"events":[],"methods":[]}},"15":{"name":"slot16","type":{"events":[],"methods":[]}},"16":{"name":"slot17","type":{"events":[],"methods":[]}},"17":{"name":"slot18","type":{"events":[],"methods":[]}},"18":{"name":"slot19","type":{"events":[],"methods":[]}},"19":{"name":"slot20","type":{"events":[],"methods":[]}},"20":{"name":"radar_1","type":{"events":[],"methods":[]}},"-1":{"name":"unit","type":{"events":[],"methods":[]}},"-2":{"name":"system","type":{"events":[],"methods":[]}},"-3":{"name":"library","type":{"events":[],"methods":[]}}},"handlers":[{"code":"--system.print(database. getConstructName(id))\npos = radar_1.getConstructPos(id)\nsystem.print(string.format(\"%s - %s (size %s) type: %s just entered the radar range at %.4f / %.4f / %.4f / \", id, radar_1.getConstructName(id), radar_1.getConstructSize(id), radar_1.getConstructType(id), pos[1], pos[2], pos[3]))\n data = {}\n    data[\"id\"] = id\n    dataIntern ={}\n    data[\"unit\"] = dataIntern\n    dataIntern[\"pos\"] = radar_1.getConstructPos(id)\n    dataIntern[\"name\"] = radar_1.getConstructName(id)\n    dataIntern[\"size\"] = radar_1.getConstructSize(id)\n    dataIntern[\"type\"] = radar_1.getConstructType(id)\n    dataIntern[\"state\"] = 1\n    \n    db.setStringValue(id, json.encode(data))","filter":{"args":[{"variable":"*"}],"signature":"enter(id)","slotKey":"20"},"key":"0"},{"code":"--db.setStringValue(id, json.encode(data))\nsystem.print(id .. \" just left the radar range\")\n\n data = {}\n    data[\"id\"] = id\n    dataIntern ={}\n    data[\"unit\"] = dataIntern\n    dataIntern[\"pos\"] = radar_1.getConstructPos(id)\n    dataIntern[\"name\"] = radar_1.getConstructName(id)\n    dataIntern[\"size\"] = radar_1.getConstructSize(id)\n    dataIntern[\"type\"] = radar_1.getConstructType(id)\n    dataIntern[\"state\"] = 0\n    \n    db.setStringValue(id, json.encode(data))","filter":{"args":[{"variable":"*"}],"signature":"leave(id)","slotKey":"20"},"key":"1"},{"code":"-- Proxy array to access auto-plugged slots programmatically\n\nweapon = {}\nweapon_size = 0\n\nradar = {}\nradar[1] = radar_1\nradar_size = 1\n-- End of auto-generated code\n\n","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"2"},{"code":"unit.setTimer(\"updateui\", 1)\n--db.clear()","filter":{"args":[],"signature":"start()","slotKey":"-1"},"key":"3"},{"code":"--[[entries = radar_1.getEntries()\n\nfor k,v in pairs(entries) do\n    data = {}\n    data[\"id\"] = v\n    dataIntern ={}\n    data[\"unit\"] = dataIntern\n    dataIntern[\"pos\"] = radar_1.getConstructPos()\n    dataIntern[\"name\"] = radar_1.getConstructName()\n    dataIntern[\"size\"] = radar_1.getConstructSize()\n    dataIntern[\"type\"] = radar_1.getConstructType()\n    \n    db.setStringValue(v, json.encode(data))\n    \nend]]","filter":{"args":[{"value":"updateui"}],"signature":"tick(timerId)","slotKey":"-1"},"key":"4"}],"methods":[],"events":[]}
unit.setTimer("radarcheck", 1)

html = html ..  "<div>"

tick("radarcheck")

local entries = getRange()
html = html ..  "<table>"
html = html ..  "<thead>"
html = html ..  "<tr>"
html = html ..  "<th>Owner</th>"
html = html ..  "<th>Pos</th>"
html = html ..  "<th>Name</th>"
html = html ..  "<th>Acceleration</th>"
html = html ..  "<th>Velocity</th>"
html = html ..  "<th>Size</th>"
html = html ..  "</tr>"
html = html ..  "</thead>"
for i, ele in entries do
    html = html .. "<tr>"
        html = html .. "<td>"
            html = html .. system.getConstructOwner(ele)
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructPos(ele) 
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructName(ele)  
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructAcceleration(ele)
        html = html .. "</td>"
        
        html = html .. "<td>"
            html = html .. system.getConstructVelocity(ele)
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructSize(ele)  
        html = html .. "</td>"
    html = html .. "</tr>"
end

html =  html .. "<tbody>"
html = html ..  "<table>"
html = html .. "</div>"


-- radar.enter

html = html ..  "<table>"
html = html ..  "<thead>"
html = html ..  "<tr>"
html = html ..  "<th>Owner</th>"
html = html ..  "<th>Pos</th>"
html = html ..  "<th>Name</th>"
html = html ..  "<th>Acceleration</th>"
html = html ..  "<th>Velocity</th>"
html = html ..  "<th>Size</th>"
html = html ..  "</tr>"
html = html ..  "</thead>"

    html = html .. "<tr>"
        html = html .. "<td>"
            html = html .. system.getConstructOwner(ele)
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructPos(ele) 
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructName(ele)  
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructAcceleration(ele)
        html = html .. "</td>"
        
        html = html .. "<td>"
            html = html .. system.getConstructVelocity(ele)
        html = html .. "</td>"

        html = html .. "<td>"
            html = html .. system.getConstructSize(ele)  
        html = html .. "</td>"
    html = html .. "</tr>"


html =  html .. "<tbody>"
html = html ..  "<table>"
html = html .. "</div>"




if (radar_1 and #radar_1.getEntries() > 0) then
    if hasSpaceRadar and radarPanelID == nil then
        ToggleRadarPanel()
    elseif hasAtmoRadar and radarPanelID == nil then
        ToggleRadarPanel()
    end
else
    if hasSpaceRadar and radarPanelID ~= nil then
        ToggleRadarPanel()
    elseif hasAtmoRadar and radarPanelID ~= nil then
        ToggleRadarPanel()
    end
end

--[[

    - getConstructName
    - getConstructPos
    - getConstructSize
    - getConstructType
    - getEntries
    - getRange
    - hasMatchingTransponder
    ]]


    local data = radar_1.getData():gmatch('"constructId":"[0-9]*","distance":[%d%.]*')
                    for v in data do
                        local id,distance = v:match('"constructId":"([0-9]*)","distance":([%d%.]*)') -- Wish I knew how to do this in gmatch
                        system.print(id .. " " .. distance)
                    end


-- other
--unit.setTimer("updateui", 10)
--db.clear()

function addToList(list, item)
    list[#list + 1] = item
  end
  -- or update
  function saveToDb(id) 
      item = json.decode(db.getStringValue(id))
      if item == nil or item.constructName == nil '' or item.constructName == 'unreachable' then
          system.print("new entry: "..entry)
          data = {}
          data["id"] = id
          dataIntern ={}
          data["unit"] = dataIntern
          dataIntern["pos"] = radar_1.getConstructPos(id)
          dataIntern["name"] = radar_1.getConstructName(id)
          dataIntern["size"] = radar_1.getConstructSize(id)
          dataIntern["type"] = radar_1.getConstructType(id)    
          db.setStringValue(id, json.encode(data))
      end
  end
  
  entries = radar_1.getEntries()
  for k,v in pairs (entries) do  
      saveToDb(v)
  end
      
  
  