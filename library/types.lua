types={
Engine = "engine_unit",
FuelTank = "fuel_container",
ControlUnit = "basic_control_unit",
Cockpit = "cockpit",
Core = "core",
Agg = "antigravity_generator",
Gyro = "gyro",
Weapon = "weapon",
Radar = "radar",
Periscope = "periscope"
}