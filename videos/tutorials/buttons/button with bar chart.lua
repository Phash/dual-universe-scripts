local current = 50
local current2 = 75
local current3 = 12
local idx = 1
local id = "but"
screen.clear()
local buttons = {}
function getCSS() 
    return [[<style>*, *:before, *:after {
  -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
 }

body {
  background: #00F;
}

h2 {
  margin: 0 0 20px 0;
  padding: 0 0 5px 0;
  border-bottom: 1px solid #999;
  font-family: sans-serif;
  font-weight: bold;
  color: #0F0;
}

.container {
  width: 90%;
  margin: 5%;
  background: #fff;
  padding: 5%;
  overflow: hidden;
  float: left;
}
.horizontal .progress-fill {
    position: relative;
    background: #666;
    height: 60px;
    color: #f00;
    text-align: center;
    font-family: "Lato","Verdana",sans-serif;
    font-size: 40px;
    line-height: 50px;
}
.horizontal .progress-bar {
  float: left;
  height: 80px;
  width: 100%;
  padding: 12px 0;
}

.horizontal .progress-track {
  position: relative;
  width: 100%;
  height: 70px;
  background: #ebebeb;
}

.rounded .progress-track,
.rounded .progress-fill {
  border-radius: 3px;
  box-shadow: inset 0 0 5px rgba(0,0,0,.2);
}


.rounded .progress-track,
.rounded .progress-fill {
  box-shadow: inset 0 0 5px rgba(0,0,0,.2);
  border-radius: 3px;
}</style>]]
    end

function getBaseHtml() 
    local html = getCSS()
    --sgui:createButtonArea(screen, 0.3, 0.9, 0.1, 0.1, "+", inc)
    --sgui:createButtonArea(screen, 0.6, 0.9, 0.1, 0.1, "-", dec)
    
    button1 = createButton(id .. idx, 0.3, 0.8, 0.1, 0.1, "+", inc)
    buttons[idx] = button1
    html = html .. button1
    idx = idx + 1
    button2 = createButton(id .. idx, 0.6, 0.8, 0.1, 0.1, "-", dec)
    buttons[idx] = button2
    html = html .. button2
    idx = idx + 1
    return html
    end
function getBarChart() 
    local h =[[
<div class="container horizontal rounded" >
  <h2>Bar / Progress Charts</h2>
  <div class="progress-bar horizontal">
    <div class="progress-track">
      <div class="progress-fill" style =" width: ]].. current ..[[%;">
        <span>]].. current ..[[%</span>
      </div>
    </div>
  </div>

  <div class="progress-bar horizontal">
    <div class="progress-track">
      <div class="progress-fill" style =" width: ]].. current2 ..[[%;">
        <span>]].. current2 ..[[%</span>
      </div>
    </div>
  </div>

  <div class="progress-bar horizontal">
    <div class="progress-track">
      <div class="progress-fill" style =" width: ]].. current3 ..[[%;">
        <span>]].. current3 ..[[%</span>
      </div>
    </div>
  </div>


</div>
	]]
   return h
    end
function createButton(id, x, y, hx, hy, text, fun)
    button = "<div class='bootstrap' style='background:#FF0000;width:".. (hx*100) ..
	         "vw;height:" .. (hy*100) .."vh;overflow:hidden;font-size:".. (hy*50) ..
	         "vh; position:fixed; left: "..(x*100).."vw; top:"..(y*100).."vh;'>" .. text .. "</div>"
	--id = screen.setContent(x * 100, y * 100, button)
 
    sgui:createClickableArea(id, x, y, hx, hy, fun)
    return button
end
    
function initUI()
        local html = getCSS() .. [[<div style="font-size:3em">]]
    	html = html .. getBaseHtml()
        html = html .. getBarChart() .. "</div>"
        screen.setHTML(html)
    end
function updateUI()
    local html = getCSS().. [[<div style="font-size:3em">]]
 
     for k,v in ipairs (buttons) do
        html = html .. v 
        end
     html = html ..getBarChart().. "</div>"
     screen.setHTML(html)
    end
function inc() 
   if current < 100 then current = current + 1 end
    updateUI()
end
    
function dec() 
   if current > 0 then current = current - 1 end
    updateUI()
end
   
screen.clear()
initUI()

