
-- unit.tick(upd)
-- Beispiel 1

--db.setStringValue("eins", "Hello from update")
--system.print (db.getStringValue("eins"))

-- Beispiel 2
system.print("wir holen es aus der DB")
datenFromDB = json.decode( db.getStringValue("daten"))
system.print(datenFromDB.alpha)
system.print(datenFromDB.beta)
for k,v in ipairs (datenFromDB.gamma) do
	system.print(v)
end



-- unit.start
-- Beispiel 1
--[[
variable = "Hello World from Program"
system.print (variable)
db.setStringValue("eins", variable)

eins = db.getStringValue("eins")
system.print (eins)
unit.setTimer("upd", 3)
]]

-- Beispiel 2
system.print("Beispiel 2: ")
unit.setTimer("upd", 3)
data = {}
data ["alpha"] = "hier fängt es an"
data ["beta"] = 65
	inner = {1,2,3,5}
data ["gamma"] = inner 

system.print(data["alpha"])
system.print(json.encode(data))
system.print("wir schreiben es in die DB: ")
db.setStringValue("daten", json.encode(data))



