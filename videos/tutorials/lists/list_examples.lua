list = List()

list:append(1)
list:append(3)
list:append(5)

erg = list:reduce(function (a,b) return a+b end)
system.print (erg)

multiplied = list:map(function(a) return a*10 end)
system.print (multiplied:len())

multiplied:foreach(function(a) system.print(string.format("multiplied -> %s",a)) end)

filtered = multiplied:filter(function(a) return a < 31 end)

filtered:foreach(function(a) system.print(string.format("filtered -> %s",a)) end)